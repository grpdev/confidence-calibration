import random
from .models import State, FamousPeople, GeographicObject
from questions.enums import PeopleSex, QuestionType

class Questions:
    "Класс для построения вопросов из бд"
     
    def make_one_q():
        question = []
        flag = random.choice([0, 1, 2])
        count_f = FamousPeople.objects.count()
        if flag == 0:
            fact = FamousPeople.objects.all()[random.randrange(0, count_f)]
            if fact.sex == PeopleSex.MALE:
                born = 'родился'
            elif fact.sex == PeopleSex.FEMALE:
                born = 'родилась'
            item = 'В каком году {} {} {} ?'.format(born, fact.occupation, fact.name)
            answer = str(fact.year_of_birth)
            question.append({
                'id':fact.id, 
                'item': item,
                'right_answer': answer,
                'type': QuestionType.PEOPLE
                })
        elif flag == 1:
            count_f = GeographicObject.objects.filter(type = 'Гора').count()
            fact = GeographicObject.objects.filter(type = 'Гора')[random.randrange(0, count_f)]
            item = 'Какая высота (в метрах) у горы {} ?'.format(fact.name)
            answer = str(fact.property)
            question.append({
                'id':fact.id, 
                'item': item,
                'right_answer': answer,
                'type': QuestionType.GEO
                })
        elif flag == 2:
            count_f = GeographicObject.objects.filter(type = 'Река').count()
            fact = GeographicObject.objects.filter(type = 'Река')[random.randrange(0, count_f)]
            item = 'Какая длина (в километрах) у реки {} ?'.format(fact.name)
            answer = str(fact.property)
            question.append({
                'id':fact.id, 
                'item': item,
                'right_answer': answer,
                'type': QuestionType.GEO
                })

        return question

    def make_questions():
        #count of questions
        count_questions = 10
        id_list = []
        questions = []

        for i in range(0, count_questions):
            is_exist = True
            fact = Questions.make_one_q()
            while is_exist:
                if fact[0]['id'] not in id_list:
                    is_exist = False
                else:
                    fact = Questions.make_one_q()
            id_list.append(fact[0]['id'])
            questions.append({
                'number':i+1, 
                'item': fact[0]['item'],
                'right_answer': fact[0]['right_answer'],
                'type': fact[0]['type']
                })
        return questions