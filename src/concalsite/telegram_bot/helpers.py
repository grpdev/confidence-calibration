from questions.enums import TestStatus, QuestionType
from telegram_bot.enums import ChatWaitingStatus, ChatTypeBlock
from telegram_bot.models import ChatQuestionSession, ChatQuestion

ASK_FOR_BEFORE = 'Введите начало интервала >= 0!'

def process_response(response):
    extra = {}
    if isinstance(response, (list, tuple)):
        text, extra = response[:2]
    else:
        text = response

    return text, extra


def calc_and_close(chat):
    session = ChatQuestionSession.objects.get(
        chat_id=chat.id,
        status=TestStatus.OPEN
    )

    session.status = TestStatus.CLOSE

    if chat.type_block == ChatTypeBlock.FIRST:
        session.real_confidence = real_confidence = session.calc_confidence(
            session.chatquestion_set.all()
            )
        session.specified_confidence = spec_confidence = session.calc_spec_confidence(
            session.chatquestion_set.all()
            )
    elif chat.type_block == ChatTypeBlock.SECOND:
        session.real_confidence = real_confidence = session.calc_confidence_second(
            session.chatquestion_set.all()
            )

    session.save()

    chat.wait_for = ChatWaitingStatus.START
    chat.save()

    text ='Спасибо за участие! Результат теста: ваша реальная уверенность = {} %.\n'.format(real_confidence)

    if chat.type_block == ChatTypeBlock.FIRST:
        text+='Указанная вами уверенность = {} %.'.format(spec_confidence)

    return text


def parse_confidence(text):
    try:
        clear_value = int(text)
    except ValueError:
        return None

    if 50 <= clear_value <= 100:
        return clear_value


def create_question_and_response(session_id, type_block):
    questions = ChatQuestion.objects.filter(session=session_id)
    is_exist = True

    if type_block == ChatTypeBlock.FIRST:
        question = ChatQuestion.make_one()
    elif type_block == ChatTypeBlock.SECOND:
        question = ChatQuestion.make_second()

    while is_exist:
        flag = False
        for quest in questions:
            if quest.item == question.item:
                flag = True

                if type_block == ChatTypeBlock.FIRST:
                    question = ChatQuestion.make_one()
                elif type_block == ChatTypeBlock.SECOND:
                    question = ChatQuestion.make_second()

                break
        is_exist = flag

    question.session_id = session_id
    question.save()

    if type_block == ChatTypeBlock.FIRST:
        return question.make_response()
    elif type_block == ChatTypeBlock.SECOND:
        message = '{}\n{}'.format(question.item, ASK_FOR_BEFORE)
        return message


def create_story_of_one_test(session, number):
    text='Тест № {}\n\n'.format(number)
    questions = session.chatquestion_set.all()

    for j, question in enumerate(questions):
        text+='{}. {} \n'.format(j+1, question.item)
        text+='Правильный ответ: {} \n'.format(question.right_answer)
        text+='Ваш ответ: {} \n\n'.format(question.user_answer)
        if question.type == QuestionType.STATE:
                text+='Ваша уверенность: {} % \n\n'.format(question.user_confidence)

    if question.type == QuestionType.STATE:
            text+='\nРезультат теста: указанная вами уверенность = {} %.\n'.format(session.specified_confidence)
            text+='\nВаша реальная уверенность = {} %.\n\n'.format(session.real_confidence)
    else:
        text+='\nРезультат теста: ваша реальная уверенность = {} %.\n'.format(session.real_confidence)
    return text

def on_create_story(chat_id, params=None):
    story = []
    is_exists = ChatQuestionSession.objects.filter(
        chat_id=chat_id)
    if not is_exists:
        return story
    else:
        story = []
        sessions = ChatQuestionSession.objects.filter(chat_id=chat_id).order_by('-create_date')[:5]
        for i, session in enumerate(sessions):
            temp = create_story_of_one_test(session, i+1)
            story.append(temp)
        return story
