# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0004_questionsession_user'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='famouspeople',
            options={'verbose_name': 'Известная персона', 'verbose_name_plural': 'Известные персоны'},
        ),
        migrations.AlterModelOptions(
            name='geographicobject',
            options={'verbose_name': 'Географический объект', 'verbose_name_plural': 'Географические объекты'},
        ),
        migrations.AlterModelOptions(
            name='state',
            options={'verbose_name': 'Государство', 'verbose_name_plural': 'Страны'},
        ),
        migrations.AlterField(
            model_name='questionsession',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, on_delete=django.db.models.deletion.PROTECT),
        ),
    ]
