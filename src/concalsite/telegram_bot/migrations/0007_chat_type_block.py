# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('telegram_bot', '0006_chatquestionsession_specified_confidence'),
    ]

    operations = [
        migrations.AddField(
            model_name='chat',
            name='type_block',
            field=models.IntegerField(default=1),
        ),
    ]
