from django_enumfield import enum


class QuestionType(enum.Enum):
    STATE = 1
    PEOPLE = 2
    GEO = 3


class TestStatus(enum.Enum):
    OPEN = 1
    CLOSE = 2

class PeopleSex(enum.Enum):
	MALE = 1
	FEMALE = 2
