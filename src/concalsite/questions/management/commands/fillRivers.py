from questions.models import GeographicObject
from django.core.management.base import BaseCommand
import requests

class Command(BaseCommand):

    def handle(self, *args, **options):
        url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'
        query = '''SELECT DISTINCT ?name ?len
                {
                    ?item wdt:P31 wd:Q4022;
                    p:P2043[ 
                        psv:P2043[
                            wikibase:quantityAmount ?len;
                            wikibase:quantityUnit ?unit;
                        ]
                    ];
                    p:P625 [
                        psv:P625 [
                            wikibase:geoGlobe ?globe ;
                    ];
                    ps:P625 ?coord
                    ]
                    FILTER ( ?globe = wd:Q2 )
                    FILTER ( ?unit = wd:Q828224 )
                    SERVICE wikibase:label {
                        bd:serviceParam wikibase:language "ru" .
                        ?item rdfs:label ?name
                    }
                }
                LIMIT 4000
                '''
        data = requests.get(url, params={'query': query, 'format': 'json'}).json()
        for item in data['results']['bindings']:
            if(item['name']['value'][0] != "Q"):
                GeographicObject.objects.update_or_create(
                    name= item['name']['value'], 
                    type = "Река", 
                    property = item['len']['value']
                )