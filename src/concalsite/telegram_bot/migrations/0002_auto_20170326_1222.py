# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('telegram_bot', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('wait_for', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='ChatQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('item', models.TextField(verbose_name='Вопрос')),
                ('right_answer', models.CharField(max_length=10)),
                ('user_answer', models.TextField(default='')),
                ('user_confidence', models.IntegerField(default=0)),
                ('type', models.IntegerField(default=1)),
                ('create_date', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ChatQuestionSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('real_confidence', models.DecimalField(default=0, max_digits=5, decimal_places=2)),
                ('status', models.IntegerField(default=2)),
                ('chat', models.ForeignKey(to='telegram_bot.Chat')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='chattestsession',
            name='question_session',
        ),
        migrations.DeleteModel(
            name='ChatTestSession',
        ),
        migrations.AddField(
            model_name='chatquestion',
            name='session',
            field=models.ForeignKey(to='telegram_bot.ChatQuestionSession'),
        ),
    ]
