# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FamousPeople',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=100)),
                ('year_of_birth', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='GeographicObject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=100)),
                ('type', models.CharField(max_length=6)),
                ('property', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('item', models.TextField()),
                ('right_answer', models.CharField(max_length=10)),
                ('user_answer', models.CharField(max_length=10)),
                ('user_confidence', models.IntegerField()),
                ('type', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='QuestionSession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('real_confidence', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(max_length=100)),
                ('head', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='question',
            name='session',
            field=models.ForeignKey(to='questions.QuestionSession'),
        ),
    ]
