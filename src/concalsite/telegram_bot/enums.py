from django_enumfield import enum


class ChatWaitingStatus(enum.Enum):
    START = 1
    ANSWER = 2
    CONFIDENCE = 3
    BEFORE = 4
    AFTER = 5

class ChatTypeBlock(enum.Enum):
	NONE = 1
	FIRST = 2
	SECOND = 3
