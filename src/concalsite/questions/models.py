import random

from django.db import models
from django.conf import settings
from django_enumfield import enum

from questions.enums import QuestionType, TestStatus, PeopleSex

from questions.parse import (
    parse_before,
    parse_after
    )

class ImportantManager(models.Manager):
    def get_queryset(self):
        return super(ImportantManager, self).get_queryset().filter(status=True)

class State(models.Model):
    type = QuestionType.STATE

    name = models.CharField(max_length=100, verbose_name='Наименование')
    head = models.CharField(max_length=100, verbose_name='Глава')
    status = models.BooleanField(default = True, verbose_name='Статус')
    objects_all = models.Manager()
    objects = ImportantManager()

    def __str__(self):
        return self.name + ", " + self.head

    class Meta:
        verbose_name = 'Государство'
        verbose_name_plural = 'Страны'

    @classmethod
    def create_question(cls):
        count = cls.objects.count()
        truth = random.choice([0, 1])
        first = cls.objects.all()[random.randrange(0, count)]

        head = first.head
        state = first.name
        right = 'да'  # FIXME хардкод
        if not truth:
            second = cls.objects.all()[random.randrange(0, count)]
            state = second.name
            right = 'нет' if first.name != second.name else 'да'

        return {
            'question': '{head} является главой государства {state}?'.format(
                head=head,
                state=state
            ),
            'right': right,
            'type': cls.type
        }


class FamousPeople(models.Model):
    type = QuestionType.PEOPLE
    name = models.CharField(max_length=100, verbose_name='Имя')
    year_of_birth = models.IntegerField(verbose_name='Год рождения')
    sex = enum.EnumField(PeopleSex,
                              default=PeopleSex.MALE)
    occupation = models.CharField(max_length=50,verbose_name='Род деятельности',
                                     default='')
    status = models.BooleanField(default = True, verbose_name='Статус')
    objects_all = models.Manager()
    objects = ImportantManager()

    def __str__(self):
        return self.name + ", " + str(self.year_of_birth) + ", " + str(self.occupation)

    class Meta:
        verbose_name = 'Известная персона'
        verbose_name_plural = 'Известные персоны'


class GeographicObject(models.Model):
    type = QuestionType.GEO
    name = models.CharField(max_length=100, verbose_name='Наименование')
    # TODO: лучше Enum, переименовать
    type = models.CharField(max_length=6, verbose_name='Тип')
    # FIXME: переименовать
    property = models.FloatField(verbose_name='Высота(м) или длина(км)')
    status = models.BooleanField(default = True, verbose_name='Статус')
    objects_all = models.Manager()
    objects = ImportantManager()

    def __str__(self):
        return self.name + ", " + str(self.property)

    class Meta:
        verbose_name = 'Географический объект'
        verbose_name_plural = 'Географические объекты'


class BaseQuestionSession(models.Model):
    real_confidence = models.DecimalField(max_digits=5, decimal_places=2,
                                          default=0)
    specified_confidence = models.DecimalField(max_digits=5, decimal_places=2,
                                          default=0)
    status = enum.EnumField(TestStatus, default=TestStatus.CLOSE)
    create_date = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        abstract = True

    @classmethod
    def calc_confidence(cls, questions):
        count = 0
        total = 0
        for question in questions:
            count += 1
            if question.user_answer == question.right_answer:
                total += 1 if question.user_confidence > 50 else 0.5

        return total / count * 100 if count else 0

    @classmethod
    def calc_spec_confidence(cls, questions):
        count = 0
        total = 0
        for question in questions:
            count += 1
            if question.user_answer == question.right_answer:
                total += 1

        return total / count * 100 if count else 0

    @classmethod
    def calc_right_answer(cls, question):
        temp = question.user_answer.split('-', maxsplit = 2) 
        before = after = ''
        i = 0
        for el in temp:
            if not i:
                before = temp[i]
            else:
                after = temp[i]
            i = i + 1
        clear_value = parse_before(before)
        if not clear_value:
            before = '0'
        clear_value = parse_after(after)
        if not clear_value:
            after = '0'
        if float(before) <= float(question.right_answer) <= float(after):
            return True
        else:
            return False
    
    @classmethod
    def calc_confidence_second(cls, questions):
        count = 0
        total = 0
        for question in questions:
            count += 1
            if cls.calc_right_answer(question):
                total += 1
                
        return total / count * 100 if count else 0


class MetaQuestion(models.base.ModelBase):
    def __new__(cls, name, bases, attrs):
        session_model = attrs.get('session_model')
        if session_model:
            attrs['session'] = models.ForeignKey(session_model)
        return super().__new__(cls, name, bases, attrs)


class BaseQuestion(models.Model, metaclass=MetaQuestion):
    session_model = None
    item = models.TextField(verbose_name='Вопрос')
    right_answer = models.CharField(max_length=10)
    user_answer = models.TextField(default='')
    user_confidence = models.IntegerField(default=0)
    type = enum.EnumField(QuestionType)
    number = models.IntegerField(default=0)

    class Meta:
        abstract = True


class QuestionSession(BaseQuestionSession):
    # TODO: ForeignKey to USER
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True)


class Question(BaseQuestion):
    session_model = QuestionSession

    @classmethod
    def make_question(cls):
        question = State.create_question()

        return cls(
            item=question['question'],
            right_answer=question['right'],
            type=question['type']
        )
