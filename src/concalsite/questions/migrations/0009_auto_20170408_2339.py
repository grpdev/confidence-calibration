# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0008_questionsession_specified_confidence'),
    ]

    operations = [
        migrations.AddField(
            model_name='famouspeople',
            name='occupation',
            field=models.CharField(verbose_name='Род деятельности', default='', max_length=50),
        ),
        migrations.AddField(
            model_name='famouspeople',
            name='sex',
            field=models.IntegerField(default=1),
        ),
    ]
