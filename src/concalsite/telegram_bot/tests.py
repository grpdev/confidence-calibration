from django.test import TestCase
from django.core import management
from telepot.exception import TelegramError

from telegram_bot.enums import ChatWaitingStatus, ChatTypeBlock
from telegram_bot import handlers
from telegram_bot.models import Chat, ChatQuestion, ChatQuestionSession
from telegram_bot.views import TelegramBot

from questions.helpers import Questions
from telegram_bot.helpers import on_create_story


# Create your tests here.
class TelegramServiceTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        management.call_command('fillStateData')
        management.call_command('fillFamousPeople')
        management.call_command('fillGeographicObject')
        management.call_command('fillRivers')

    def test_availability(self):
        try:
            TelegramBot.getMe()
        except TelegramError:
           self.fail("Improperly Configured Telegram service!")

    def test_enroll_first(self):
        chat_id = 88
        chat_query = Chat.objects.filter(id=chat_id)
        hello_text = 'Выберите тип вопросов для прохождения теста: \n'
        hello_text += '1. Вам нужно будет определить, является ли утверждение верным или не верным. И указать вашу уверенность в процентах от 50 до 100.\n'
        hello_text += '2. Вопросы с 90-процентным доверительным интервалом. Отвечая на каждый вопрос, укажите нижнюю и верхнюю границы интервала.'
        self.assertEqual(handlers.on_start(chat_id), hello_text)

        chat = chat_query.get()
        self.assertEqual(chat.type_block, ChatTypeBlock.NONE)

        self.assertEqual(handlers.on_text(chat_id, 'smth'), 'Выберите номер типа вопросов: "1" или "2"!')
        self.assertEqual(handlers.on_text(chat_id, '1'), 'Для прохождения теста выполните команду /enroll')
        self.assertEqual(handlers.on_text(chat_id, '7'), 'Для прохождения теста выполните команду /enroll')

        chat = chat_query.get()
        self.assertEqual(chat.type_block, ChatTypeBlock.FIRST)

        self.assertEqual(handlers.on_last_result(chat_id), 'Вы не проходили ни одного теста!')

        answer = handlers.on_enroll(chat_id)
        chat = chat_query.get()
        self.assertEqual(chat.wait_for, ChatWaitingStatus.ANSWER)
        self.assertEqual(chat.chatquestionsession_set.count(), 1)
        self.assertIsInstance(answer, tuple)

        text, keyboard = answer
        self.assertGreater(len(text), 1)

        answer = handlers.on_enroll(chat_id)
        self.assertEqual(answer, 'Тест уже начался!')

        self.assertEqual(handlers.on_text(chat_id, 'smth'), handlers.ASK_FOR_ANSWER)
        handlers.on_text(chat_id, 'smth')
        self.assertEqual(handlers.on_text(chat_id, 'да'), handlers.ASK_FOR_CONFIDENCE)

        chat = chat_query.get()
        self.assertEqual(chat.wait_for, ChatWaitingStatus.CONFIDENCE)

        self.assertEqual(handlers.on_text(chat_id, '101'), handlers.ASK_FOR_CONFIDENCE)
        self.assertNotEqual(handlers.on_text(chat_id, '99'), handlers.ASK_FOR_CONFIDENCE)

        chat = chat_query.get()
        self.assertEqual(chat.wait_for, ChatWaitingStatus.ANSWER)

        message = 'Для того чтобы начать новый тест, завершите текущий, выполнив команду /stop!'
        self.assertEqual(handlers.on_start(chat_id), message)
        chat = chat_query.get()
        self.assertEqual(chat.wait_for, ChatWaitingStatus.ANSWER)
        self.assertEqual(chat.type_block, ChatTypeBlock.FIRST)

        handlers.on_stop(chat_id)
        self.assertEqual(handlers.on_stop(chat_id), 'Тест еще не начался!')
        handlers.on_last_result(chat_id)

    '''
    def test_not_duplicate_question(self):
        chat_id = 77
        handlers.on_start(chat_id)
        first_question, _ = handlers.on_enroll(chat_id)

        handlers.on_text(chat_id, 'да')
        second_question, _ = handlers.on_text(chat_id, 78)
        self.assertNotEqual(first_question, second_question)

    def test_wrong_date_create(self):
        chat_id = 77
        handlers.on_start(chat_id)
        handlers.on_enroll(chat_id)

        for i in range(10):
            handlers.on_text(chat_id, 'да')
            response = handlers.on_text(chat_id, '90')
            if i == 8:
                question = response[0]

        self.assertEqual(ChatQuestion.objects.latest('create_date').item, question)
    '''

    def test_enroll_second(self):
        chat_id = 31
        chat_query = Chat.objects.filter(id=chat_id)
        hello_text = 'Выберите тип вопросов для прохождения теста: \n'
        hello_text += '1. Вам нужно будет определить, является ли утверждение верным или не верным. И указать вашу уверенность в процентах от 50 до 100.\n'
        hello_text += '2. Вопросы с 90-процентным доверительным интервалом. Отвечая на каждый вопрос, укажите нижнюю и верхнюю границы интервала.'
        self.assertEqual(handlers.on_start(chat_id), hello_text)

        chat = chat_query.get()
        self.assertEqual(chat.type_block, ChatTypeBlock.NONE)

        self.assertEqual(handlers.on_text(chat_id, 'smth'), 'Выберите номер типа вопросов: "1" или "2"!')
        self.assertEqual(handlers.on_text(chat_id, '2'), 'Для прохождения теста выполните команду /enroll')
        self.assertEqual(handlers.on_text(chat_id, '5'), 'Для прохождения теста выполните команду /enroll')

        chat = chat_query.get()
        self.assertEqual(chat.type_block, ChatTypeBlock.SECOND)

        self.assertEqual(handlers.on_last_result(chat_id), 'Вы не проходили ни одного теста!')

        answer = handlers.on_enroll(chat_id)
        chat = chat_query.get()
        self.assertEqual(chat.wait_for, ChatWaitingStatus.BEFORE)
        self.assertEqual(chat.chatquestionsession_set.count(), 1)

        self.assertEqual(handlers.on_text(chat_id, 'smth'), handlers.ASK_FOR_BEFORE)
        self.assertEqual(handlers.on_text(chat_id, '-90'), handlers.ASK_FOR_BEFORE)
        self.assertEqual(handlers.on_text(chat_id, '1900'), handlers.ASK_FOR_AFTER)

        chat = chat_query.get()
        self.assertEqual(chat.wait_for, ChatWaitingStatus.AFTER)

        self.assertEqual(handlers.on_text(chat_id, 'smth'), handlers.ASK_FOR_AFTER)
        self.assertEqual(handlers.on_text(chat_id, '0'), handlers.ASK_FOR_AFTER)
        self.assertNotEqual(handlers.on_text(chat_id, '4000'), handlers.ASK_FOR_AFTER)

        chat = chat_query.get()
        self.assertEqual(chat.wait_for, ChatWaitingStatus.BEFORE)


        session = chat.chatquestionsession_set.all()[0]
        question = session.chatquestion_set.all()

        self.assertEqual(question[0].user_answer, '1900-4000')

        answer = handlers.on_enroll(chat_id)
        self.assertEqual(answer, 'Тест уже начался!')

        message = 'Для того чтобы начать новый тест, завершите текущий, выполнив команду /stop!'
        self.assertEqual(handlers.on_start(chat_id), message)
        chat = chat_query.get()
        self.assertEqual(chat.wait_for, ChatWaitingStatus.BEFORE)
        self.assertEqual(chat.type_block, ChatTypeBlock.SECOND)

        handlers.on_stop(chat_id)
        handlers.on_last_result(chat_id)

        self.assertEqual(handlers.on_stop(chat_id), 'Тест еще не начался!')

    def test_history(self):
        chat_id = 45
        chat = Chat.objects.get_or_create(
            id=chat_id
            )
        for count in range(0, 3):
            session = ChatQuestionSession(chat_id=chat_id)
            session.save()
            for i in range(0, 10):
                question = ChatQuestion.make_second()
                question.session_id = session.id
                question.save()

        for s in on_create_story(chat_id):
            print(s)
        self.assertEqual(1, 1)
