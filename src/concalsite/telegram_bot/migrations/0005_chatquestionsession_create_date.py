# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('telegram_bot', '0004_chatquestion_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatquestionsession',
            name='create_date',
            field=models.DateTimeField(null=True, auto_now_add=True),
        ),
    ]
