# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('telegram_bot', '0003_auto_20170326_1733'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatquestion',
            name='number',
            field=models.IntegerField(default=0),
        ),
    ]
