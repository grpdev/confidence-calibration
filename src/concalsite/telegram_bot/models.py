from django.db import models
from django_enumfield import enum
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton

from questions.models import BaseQuestionSession, BaseQuestion, State
from telegram_bot.enums import ChatWaitingStatus, ChatTypeBlock

from questions.helpers import Questions


class Chat(models.Model):
    id = models.BigIntegerField(primary_key=True)
    type_block = enum.EnumField(ChatTypeBlock,
                                default=ChatTypeBlock.NONE)
    wait_for = enum.EnumField(ChatWaitingStatus,
                              default=ChatWaitingStatus.START)


class ChatQuestionSession(BaseQuestionSession):
    """
    Для каждого чата должна быть одна открытая сессия
    """
    chat = models.ForeignKey(Chat)


class ChatQuestion(BaseQuestion):
    session_model = ChatQuestionSession
    create_date = models.DateTimeField(auto_now_add=True)

    @classmethod
    def make_one(cls):
        question = State.create_question()

        return cls(
            item=question['question'],
            right_answer=question['right'],
            type=question['type']
        )

    @classmethod
    def make_second(cls):
        question = Questions.make_one_q()

        return cls(
            item=question[0]['item'],
            right_answer=question[0]['right_answer'],
            type = question[0]['type']
        )

    def make_response(self):
        keyboard = ReplyKeyboardMarkup(keyboard=[
            [KeyboardButton(text='да'), KeyboardButton(text='нет')]
        ],
            one_time_keyboard=True
        )
        return self.item, {'reply_markup': keyboard}

