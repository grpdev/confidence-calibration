from django.contrib import admin
from .models import State, FamousPeople, GeographicObject

def make_published(modeladmin, request, queryset):
    queryset.update(status=False)

def make_import(modeladmin, request, queryset):
    queryset.update(status=True)

make_import.short_description = "Пометить как важные"
make_published.short_description = "Пометить как неважные"

@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    list_display = ('name', 'head', 'status')
    search_fields = ('name', 'head', 'status')
    actions = [make_published, make_import]

@admin.register(FamousPeople)
class FamousPeopleAdmin(admin.ModelAdmin):
    list_display = ('name', 'year_of_birth', 'occupation', 'status')
    search_fields = ('name', 'year_of_birth', 'occupation', 'status')
    actions = [make_published, make_import]

@admin.register(GeographicObject)
class GeographicObjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'property', 'status')
    search_fields = ('name', 'type', 'property', 'status')
    actions = [make_published, make_import]