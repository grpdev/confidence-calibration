from ..models import State, FamousPeople, GeographicObject
import random

class Questions:
    "Класс для построения вопросов из бд"

    def make_question(cls):
        question = State.create_question()
        
        return cls(
            item=question['question'],
            right_answer=question['right'],
            type=question['type']
        )

    def get_list_questions_first():
        #count of questions
        count_questions = 10

        #get a list of all State`s id
        id_list_corr = State.objects.values_list('id', flat = True)

        #get random State`s id
        rand_id_list = random.sample(list(id_list_corr), count_questions)

        questions = []

        for i in range(0, count_questions):
            right_answer = ""
            fact = State.objects.filter(id = rand_id_list[i])
            country_name = fact.values_list('name', flat = True)
            flag = random.choice([0, 1])
            if (flag == 0):
                president = fact.values_list('head', flat = True)
                right_answer = "да"
            else:
                rand = random.choice(id_list_corr)
                other_fact = State.objects.filter(id = rand)
                president = other_fact.values_list('head', flat = True)
                if(fact.values_list('name', flat = True)[0] == other_fact.values_list('name', flat = True)[0]):
                    right_answer = "да"
                else:
                    right_answer = "нет"
            questions.append({'num':i+1, 'name':country_name[0], 'head':president[0], 'answer':right_answer})

        return questions

    def get_list_questions_second():
        #count of questions
        count_questions = 10

        id_list_people = FamousPeople.objects.values_list('id', flat = True)
        id_list_geograph = GeographicObject.objects.values_list('id', flat = True)

        questions = []

        for i in range(0, count_questions):
            flag = random.choice([0, 1])
            if (flag == 0):
                rand = random.choice(id_list_people)
                fact = FamousPeople.objects.filter(id = rand)
                questions.append({'num':i+1, 'type':"people", 'name': fact.values_list('name', flat = True)[0], 'year':fact.values_list('year_of_birth', flat = True)[0]})
            else:
                rand = random.choice(id_list_geograph)
                fact = GeographicObject.objects.filter(id = rand)
                questions.append({'num':i+1, 'type':"geograph", 'name': fact.values_list('name', flat = True)[0], 'property':fact.values_list('property', flat = True)[0]})

        return questions        
