class Calculate:
    "Класс для вычисления уверенности пользователей"
    @staticmethod
    def calculate_confidence_type1(count_correct_answer, count_conf_interval, count_questions):
        sum = count_correct_answer + 0.5 * count_conf_interval
        confidence = (sum/count_questions) * 100
        return confidence

    @staticmethod
    def calculate_confidence_type2(count_correct_answer, count_questions):
        if count_correct_answer == count_questions:
            confidence = 90
        else: 
            confidence = (count_correct_answer/count_questions) * 90
        return confidence
