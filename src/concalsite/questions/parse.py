def parse_confidence(text):
    try:
        clear_value = int(text)
    except ValueError:
        return None

    if 50 <= clear_value <= 100:
        return clear_value

def parse_before(text):
    try:
        clear_value = int(text)
    except ValueError:
        return None

    if  clear_value >= 0:
        return clear_value

def parse_after(text):
    try:
        clear_value = int(text)
    except ValueError:
        return None

    if  clear_value > 0:
        return clear_value

def parse_type_block(text):
    try:
        clear_value = int(text)
    except ValueError:
        return None

    if clear_value == 1 or clear_value == 2:
        return clear_value

def parse_answer(text):
    if text == 'да' or text == 'нет':
        return text
    else:
        return None