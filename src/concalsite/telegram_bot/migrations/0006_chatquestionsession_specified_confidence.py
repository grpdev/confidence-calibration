# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('telegram_bot', '0005_chatquestionsession_create_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatquestionsession',
            name='specified_confidence',
            field=models.DecimalField(max_digits=5, default=0, decimal_places=2),
        ),
    ]
