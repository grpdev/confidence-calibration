"""
Функции обработчики должны возвращать что-то из этого:
1) Текст сообщения
2) Кортеж: Текст сообщения, словарь параметов(которые можно подставить в sendMessage)
"""
from django.db.transaction import atomic
from questions.enums import TestStatus
from telegram_bot.enums import ChatWaitingStatus, ChatTypeBlock
from questions.enums import QuestionType
from telegram_bot.helpers import (
    calc_and_close,
    create_question_and_response
    )
from questions.parse import (
    parse_confidence,
    parse_type_block,
    parse_before,
    parse_after,
    parse_answer
    )
from telegram_bot.models import Chat, ChatQuestionSession, ChatQuestion


ASK_FOR_ANSWER = 'Ваш ответ должен быть только "да" или "нет"!'
ASK_FOR_CONFIDENCE = 'Введите вашу уверенность(50 до 100)'
ASK_FOR_BEFORE = 'Введите начало интервала >= 0!'
ASK_FOR_AFTER = 'Введите конец интервала > 0!'


def on_start(chat_id, params=None):
    chat, created = Chat.objects.get_or_create(
        id=chat_id,
        defaults={'wait_for': ChatWaitingStatus.START}
    )

    if chat.wait_for == ChatWaitingStatus.START:
        chat.type_block = ChatTypeBlock.NONE
        chat.save()

        hello_text = 'Выберите тип вопросов для прохождения теста: \n'
        hello_text += '1. Вам нужно будет определить, является ли утверждение верным или не верным. И указать вашу уверенность в процентах от 50 до 100.\n'
        hello_text += '2. Вопросы с 90-процентным доверительным интервалом. Отвечая на каждый вопрос, укажите нижнюю и верхнюю границы интервала.'
        return hello_text
    else:
        message = 'Для того чтобы начать новый тест, завершите текущий, выполнив команду /stop!'
        return message


def on_help(chat_id, params=None):
    return 'Список доступных комманд'


@atomic
def on_enroll(chat_id, params=None):

    chat = Chat.objects.select_for_update().get(id=chat_id)

    if chat.wait_for != ChatWaitingStatus.START:
        return 'Тест уже начался!'

    if chat.type_block == ChatTypeBlock.SECOND:
        chat.wait_for = ChatWaitingStatus.BEFORE
    else:
        chat.wait_for = ChatWaitingStatus.ANSWER
    chat.save()

    session = ChatQuestionSession(chat_id=chat_id, status=TestStatus.OPEN)
    session.save()

    return create_question_and_response(session.id, chat.type_block)


@atomic
def on_stop(chat_id, params=None):
    chat = Chat.objects.select_for_update().get(id=chat_id)

    if chat.wait_for == ChatWaitingStatus.START:
        return 'Тест еще не начался!'

    return calc_and_close(chat)


@atomic
def on_text(chat_id, text):
    chat = Chat.objects.select_for_update().get(id=chat_id)

    wait_for = chat.wait_for
    if wait_for == ChatWaitingStatus.START and chat.type_block == ChatTypeBlock.NONE:
        type_block = parse_type_block(text)
        if not type_block:
            return 'Выберите номер типа вопросов: "1" или "2"!'
        if type_block == 1:
            chat.type_block = ChatTypeBlock.FIRST
        else:
            chat.type_block = ChatTypeBlock.SECOND
        chat.save()
        return 'Для прохождения теста выполните команду /enroll'

    elif wait_for == ChatWaitingStatus.START:
        return 'Для прохождения теста выполните команду /enroll'


    question_query = ChatQuestion.objects.filter(
        session__chat=chat_id,
        session__status=TestStatus.OPEN
        )

    if chat.type_block == ChatTypeBlock.FIRST:
        response = on_first_test(chat, question_query, text)

    elif chat.type_block == ChatTypeBlock.SECOND:
        response = on_second_test(chat, question_query, text)

    return response

def on_first_test(chat, question_query, text):
    current_question = question_query.latest('create_date')
    wait_for = chat.wait_for

    if wait_for == ChatWaitingStatus.ANSWER:
        clear_value = parse_answer(text)
        if not clear_value:
            return ASK_FOR_ANSWER
        current_question.user_answer = text
        chat.wait_for = ChatWaitingStatus.CONFIDENCE
        response = ASK_FOR_CONFIDENCE
    elif wait_for == ChatWaitingStatus.CONFIDENCE:
        clear_value = parse_confidence(text)
        if not clear_value:
            return ASK_FOR_CONFIDENCE
        current_question.user_confidence = clear_value
        chat.wait_for = ChatWaitingStatus.ANSWER
        if question_query.count() < 10:
            response = create_question_and_response(current_question.session_id, chat.type_block)
        else:
            response = calc_and_close(chat)

    chat.save()
    current_question.save()

    return response

def on_second_test(chat, question_query, text):
    current_question = question_query.latest('create_date')
    wait_for = chat.wait_for
    user_before = ''
    user_after = ''

    if wait_for == ChatWaitingStatus.BEFORE:
        clear_value = parse_before(text)
        if not clear_value:
            return ASK_FOR_BEFORE
        user_before = clear_value
        current_question.user_answer = '{}-'.format(user_before)
        chat.wait_for = ChatWaitingStatus.AFTER
        response = ASK_FOR_AFTER
    elif wait_for == ChatWaitingStatus.AFTER:
        clear_value = parse_after(text)
        if not clear_value:
            return ASK_FOR_AFTER
        user_after = clear_value
        current_question.user_answer += '{}'.format(user_after)
        chat.wait_for = ChatWaitingStatus.BEFORE

        if question_query.count() < 10:
            response = create_question_and_response(current_question.session_id, chat.type_block)
        else:
            response = calc_and_close(chat)

    chat.save()
    current_question.save()

    return response

def on_last_result(chat_id, params=None):
    is_exists = ChatQuestionSession.objects.filter(
        chat_id=chat_id)
    if not is_exists:
        return 'Вы не проходили ни одного теста!'
    else:
        session = ChatQuestionSession.objects.filter(chat_id=chat_id).order_by('-create_date').values()[0]
        questions = ChatQuestion.objects.filter(session=session['id'])
        text = 'Ваш последний результат:\n\n'
        for j, question in enumerate(questions):
            text+='{}. {} \n'.format(j+1, question.item)
            text+='Правильный ответ: {} \n'.format(question.right_answer)
            text+='Ваш ответ: {} \n\n'.format(question.user_answer)
            if question.type == QuestionType.STATE:
                text+='Ваша уверенность: {} % \n\n'.format(question.user_confidence)
        if question.type == QuestionType.STATE:
            text+='\nРезультат теста: указанная вами уверенность = {} %.\n'.format(session['specified_confidence'])
            text+='\nВаша реальная уверенность = {} %.\n\n'.format(session['real_confidence'])
        else:
            text+='\nРезультат теста: ваша реальная уверенность = {} %.\n'.format(session['real_confidence'])
        return text

