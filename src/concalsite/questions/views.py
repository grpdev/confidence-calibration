from django.shortcuts import render, redirect
from .means.calculate import Calculate
from .forms import QuestionForm
from .helpers import Questions
from .parse import (
    parse_confidence, 
    parse_before, 
    parse_after
    )
from django.http import HttpResponse
from .models import QuestionSession, Question
from .enums import QuestionType

def history(request):
    session_information = []
    sessions = QuestionSession.objects.filter(user = request.user)
    for s, session in enumerate(sessions):
        test_information = []
        questions = session.question_set.all()
        for i, question in enumerate(questions):
            test_information.append({
                'num_quest': (i+1), 
                'question': question.item, 
                'right_answer': question.right_answer, 
                'user_answer':question.user_answer, 
                'user_confidence':question.user_confidence
                })
            if question.type == QuestionType.STATE:
                flag = True
            else:
                flag = False
        session_information.append({
            'num_test': (s+1), 
            'questions': test_information, 
            'real_confidence': session.real_confidence,
            'specified_confidence': session.specified_confidence,
            'date': session.create_date,
            'flag':flag
            })
    return render(request, 'history.html', {'sessions': session_information})

def questions_home(request):
     return render(request, 'questions_home.html', {})

def first(request):
        form = []
        session = QuestionSession(user = request.user)
        session.save()
        for i in range(0, 10):
            questions = Question.objects.filter(session=session.id)
            is_exist = True
            question = Question.make_question()
            while is_exist:
                flag = False
                for quest in questions:
                    if quest.item == question.item:
                        flag = True
                        question = Question.make_question()
                        break
                is_exist = flag
            question.session_id = session.id
            question. number = i+1
            question.save()
            form.append({
                'num':question.number, 
                'question': question.item
                })

        return render(request, 'first.html', {'questions':form, 'session_id': session.id})

def update_result(request, session_id):
    all_questions = []
    questions = Question.objects.filter(session=session_id).order_by('number')

    if request.method == 'POST':
        for i, question in enumerate(questions):
            user_answer = request.POST.get('answer_' + str(question.number))
            errors = []
            answer = ''
            confidence = 0
            if not user_answer:
                errors.append('Выберите ответ!')
            else:
                answer = user_answer
                if user_answer == "false":
                    question.user_answer = "нет"
                else:
                    question.user_answer = "да"

            user_confidence = request.POST.get('user_confidence_' + str(question.number))
            confidence = parse_confidence(user_confidence)
            if not confidence:
                confidence = 0
                errors.append('Введите уверенность от 50 до 100!')
            else:
                question.user_confidence = user_confidence
            
            question.save()
            all_questions.append({
                'num':question.number, 
                'question': question.item, 
                'errors': errors, 
                'answer': answer, 
                'confidence': confidence
                })
        
        if not errors:
            return redirect('result', session_id)

    return render(request, 'update_result.html', {'questions':all_questions})

def second(request):
    session = QuestionSession(user = request.user)
    session.save()
    questions = Questions.make_questions()
    for question in questions:
        Question.objects.update_or_create(
            number = question['number'],
            item = question['item'],
            right_answer = question['right_answer'],
            type = question['type'],
            session_id = session.id
            )

    return render(request, 'second.html', {'questions':questions, 'session_id': session.id})  

def update_result_2(request, session_id):
    all_questions = []
    questions = Question.objects.filter(session=session_id).order_by('number')

    if request.method == 'POST':
        for i, question in enumerate(questions):
            errors = []
            before = ''
            after = ''
            user_before = request.POST.get('user_before_' + str(question.number))
            before = parse_before(user_before)
            if not before and before != 0:
                before = ''
                errors.append('Введите начало интервала >= 0!')

            user_after = request.POST.get('user_after_' + str(question.number))
            after = parse_after(user_after)
            if not after:
                after = ''
                errors.append('Введите конец интервала > 0!')
        
            question.user_answer = '{}-{}'.format(user_before, user_after)
            question.save()
            all_questions.append({
                'num':question.number, 
                'question': question.item, 
                'errors': errors, 
                'before': before , 
                'after': after
                })
        
        if not errors:
            return redirect('result_2', session_id)

    return render(request, 'update_result_2.html', {'questions':all_questions})

def result(request, session_id):
    session = QuestionSession.objects.get(id = session_id)
    session.real_confidence = session.calc_confidence(session.question_set.all())
    session.specified_confidence = session.calc_spec_confidence(session.question_set.all())
    session_date = session.create_date
    session.save()
    return render(request, 'result.html', {'real_confidence':session.real_confidence, 
        'specified_confidence': session.specified_confidence, 'date':session_date})

def result_2(request, session_id):
    session = QuestionSession.objects.get(id = session_id)
    session.real_confidence = session.calc_confidence_second(session.question_set.all())
    session_date = session.create_date
    session.save()
    return render(request, 'result_2.html', {'confidence':session.real_confidence, 'date':session_date})

def last_result(request):
    all_questions = []
    is_exists = QuestionSession.objects.filter(user = request.user)
    spec = True
    if is_exists:
        session = QuestionSession.objects.filter(user = request.user).order_by('-create_date').values()[0]
        questions = Question.objects.filter(session=session['id'])
        
        for i, question in enumerate(questions):
            all_questions.append({
                'num_quest': (i+1),
                'question': question.item,
                'right_answer': question.right_answer, 
                'user_answer':question.user_answer, 
                'user_confidence':question.user_confidence
                })
        
            if question.type == QuestionType.STATE:
                spec = True
            else:
                spec = False
        return render(request, 'last_result.html', {'flag': is_exists,'spec':spec, 'questions': all_questions, 
            'real_confidence': session['real_confidence'],'spec_confidence': session['specified_confidence'],
            'date': session['create_date'] })
    else:
        return render(request, 'last_result.html', {'flag': is_exists})