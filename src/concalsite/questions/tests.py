import random

from django.test import TestCase

from questions.models import QuestionSession, Question
from .means.calculate import Calculate


class ConCalTestCase(TestCase):
    def test_conf_calculate_type1(self):
        self.assertEqual(Calculate.calculate_confidence_type1(5, 2, 10), 60)
        self.assertEqual(Calculate.calculate_confidence_type1(7, 0, 10), 70)

    def test_conf_calculate_type2(self):
        self.assertEqual(Calculate.calculate_confidence_type2(10, 10), 90)
        self.assertEqual(Calculate.calculate_confidence_type2(8, 10), 72)

    def test_confidence_by_questions(self):
        # Аналогия с test_conf_calculate_type1
        session = QuestionSession.objects.create()

        for i in range(5):
            Question.objects.create(
                session=session,
                user_confidence=51,
                right_answer='77',
                user_answer='77'
            )
        for i in range(2):
            Question.objects.create(
                session=session,
                user_confidence=50,
                right_answer='77',
                user_answer='77'
            )
        for i in range(3):
            Question.objects.create(
                session=session,
                user_confidence=random.randint(49, 99),
                right_answer='77',
                user_answer='88'
            )

        self.assertEqual(QuestionSession.calc_confidence(
            session.question_set.all()
        ), 60)
