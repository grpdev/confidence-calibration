# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0006_question_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionsession',
            name='create_date',
            field=models.DateTimeField(null=True, auto_now_add=True),
        ),
    ]
