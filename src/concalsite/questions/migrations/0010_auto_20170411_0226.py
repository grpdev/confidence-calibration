# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0009_auto_20170408_2339'),
    ]

    operations = [
        migrations.AddField(
            model_name='state',
            name='status',
            field=models.BooleanField(default=True, verbose_name='Статус'),
        ),
        migrations.AlterField(
            model_name='geographicobject',
            name='property',
            field=models.FloatField(verbose_name='Высота(м) или длина(км)'),
        ),
    ]
