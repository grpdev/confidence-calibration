from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

from core import views as core_views
from questions import views as questions_views


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TemplateView.as_view(template_name="home.html"), name='home'),
    url(r'^telegram/', include('telegram_bot.urls', namespace='telegram')),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'},
        name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^signup/$', core_views.signup, name='signup'),
    url(r'^history/$', questions_views.history, name='history'),
    url(r'^questions_home/$', questions_views.questions_home, name='questions_home'),
    url(r'^first/$', questions_views.first, name='first'),
    url(r'^second/$', questions_views.second, name='second'),
    url(r'^result/(?P<session_id>[0-9]+)/$', questions_views.result, name='result'),
    url(r'^result_2/(?P<session_id>[0-9]+)/$', questions_views.result_2, name='result_2'),
    url(r'^update_result/(?P<session_id>[0-9]+)/$', questions_views.update_result, name='update_result'),
    url(r'^update_result_2/(?P<session_id>[0-9]+)/$', questions_views.update_result_2, name='update_result_2'),
    url(r'^last_result/$', questions_views.last_result, name='last_result'),
    url('', include('social.apps.django_app.urls', namespace='social')),
]
