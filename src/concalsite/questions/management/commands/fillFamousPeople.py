from questions.models import FamousPeople
from django.core.management.base import BaseCommand
import requests
from questions.enums import PeopleSex


class Command(BaseCommand):

    def handle(self, *args, **options):
        url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'
        query = '''SELECT DISTINCT ?humanLabel ?year ?sexLabel
        {
            {?human wdt:P106 wd:Q36180 . }
            ?human wdt:P569 ?date .
            ?human wdt:P21 ?sex .
            BIND(YEAR(?date) as ?year)
            FILTER( ?year > 1300)
            SERVICE wikibase:label { bd:serviceParam wikibase:language "ru" }
        }
        LIMIT 1000
        '''
        data = requests.get(url, params={'query': query, 'format': 'json'}).json()
        for item in data['results']['bindings']:
            if(item['humanLabel']['value'][0] != "Q"):
                occupation =''
                sex = 1
                if item['sexLabel']['value'] == 'женский пол':
                    sex = PeopleSex.FEMALE
                    occupation = 'писательница'
                elif item['sexLabel']['value'] == 'мужской пол':
                    sex = PeopleSex.MALE
                    occupation = 'писатель'
                FamousPeople.objects.update_or_create(
                    name=item['humanLabel']['value'],
                    year_of_birth=item['year']['value'],
                    sex=sex,
                    occupation=occupation
                )

        query = '''SELECT DISTINCT ?humanLabel ?year ?sexLabel
        {
            {?human wdt:P106 wd:Q36834 . }
            ?human wdt:P569 ?date .
            ?human wdt:P21 ?sex .
            BIND(YEAR(?date) as ?year)
            FILTER( ?year > 1300)
            SERVICE wikibase:label { bd:serviceParam wikibase:language "ru" }
        }
        LIMIT 1000
        '''
        data = requests.get(url, params={'query': query, 'format': 'json'}).json()
        for item in data['results']['bindings']:
            if(item['humanLabel']['value'][0] != "Q"):
                sex = 1
                if item['sexLabel']['value'] == 'женский пол':
                    sex = PeopleSex.FEMALE
                elif item['sexLabel']['value'] == 'мужской пол':
                    sex = PeopleSex.MALE
                FamousPeople.objects.update_or_create(
                    name=item['humanLabel']['value'],
                    year_of_birth=item['year']['value'],
                    sex=sex,
                    occupation='композитор'
                )
        
        query = '''SELECT DISTINCT ?humanLabel ?year ?sexLabel
        {
            {?human wdt:P106 wd:Q1028181 . }
            ?human wdt:P569 ?date .
            ?human wdt:P21 ?sex .
            BIND(YEAR(?date) as ?year)
            FILTER( ?year > 1300)
            SERVICE wikibase:label { bd:serviceParam wikibase:language "ru" }
        }
        LIMIT 500
        '''
        data = requests.get(url, params={'query': query, 'format': 'json'}).json()
        for item in data['results']['bindings']:
            if(item['humanLabel']['value'][0] != "Q"):
                occupation =''
                sex = 1
                if item['sexLabel']['value'] == 'женский пол':
                    sex = PeopleSex.FEMALE
                    occupation = 'художница'
                elif item['sexLabel']['value'] == 'мужской пол':
                    sex = PeopleSex.MALE
                    occupation = 'художник'
                FamousPeople.objects.update_or_create(
                    name=item['humanLabel']['value'],
                    year_of_birth=item['year']['value'],
                    sex=sex,
                    occupation=occupation
                )

        query = '''SELECT DISTINCT ?humanLabel ?year ?sexLabel
        {
            {?human wdt:P106 wd:Q33999 . }
            ?human wdt:P569 ?date .
            ?human wdt:P21 ?sex .
            BIND(YEAR(?date) as ?year)
            FILTER( ?year > 1300)
            SERVICE wikibase:label { bd:serviceParam wikibase:language "ru" }
        }
        LIMIT 500
        '''
        data = requests.get(url, params={'query': query, 'format': 'json'}).json()
        for item in data['results']['bindings']:
            if(item['humanLabel']['value'][0] != "Q"):
                occupation =''
                sex = 1
                if item['sexLabel']['value'] == 'женский пол':
                    sex = PeopleSex.FEMALE
                    occupation = 'актриса'
                elif item['sexLabel']['value'] == 'мужской пол':
                    sex = PeopleSex.MALE
                    occupation = 'актер'
                FamousPeople.objects.update_or_create(
                    name=item['humanLabel']['value'],
                    year_of_birth=item['year']['value'],
                    sex=sex,
                    occupation=occupation
                )