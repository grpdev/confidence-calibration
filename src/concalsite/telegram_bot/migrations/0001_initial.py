# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0002_auto_20170323_1709'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChatTestSession',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('chat_id', models.IntegerField()),
                ('question_session', models.OneToOneField(to='questions.QuestionSession')),
            ],
        ),
    ]
