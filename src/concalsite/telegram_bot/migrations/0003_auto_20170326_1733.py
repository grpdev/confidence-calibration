# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('telegram_bot', '0002_auto_20170326_1222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chatquestion',
            name='create_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
