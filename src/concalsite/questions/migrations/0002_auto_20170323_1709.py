# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='famouspeople',
            options={'verbose_name': 'Известная персона'},
        ),
        migrations.AlterModelOptions(
            name='geographicobject',
            options={'verbose_name': 'Географический объект'},
        ),
        migrations.AlterModelOptions(
            name='state',
            options={'verbose_name': 'Государство'},
        ),
        migrations.AddField(
            model_name='questionsession',
            name='status',
            field=models.IntegerField(default=2),
        ),
        migrations.AlterField(
            model_name='famouspeople',
            name='name',
            field=models.CharField(verbose_name='Имя', max_length=100),
        ),
        migrations.AlterField(
            model_name='famouspeople',
            name='year_of_birth',
            field=models.IntegerField(verbose_name='Год рождения'),
        ),
        migrations.AlterField(
            model_name='geographicobject',
            name='name',
            field=models.CharField(verbose_name='Наименование', max_length=100),
        ),
        migrations.AlterField(
            model_name='geographicobject',
            name='type',
            field=models.CharField(verbose_name='Тип', max_length=6),
        ),
        migrations.AlterField(
            model_name='question',
            name='item',
            field=models.TextField(verbose_name='Вопрос'),
        ),
        migrations.AlterField(
            model_name='question',
            name='type',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='questionsession',
            name='real_confidence',
            field=models.DecimalField(decimal_places=2, max_digits=5),
        ),
        migrations.AlterField(
            model_name='state',
            name='head',
            field=models.CharField(verbose_name='Глава', max_length=100),
        ),
        migrations.AlterField(
            model_name='state',
            name='name',
            field=models.CharField(verbose_name='Наименование', max_length=100),
        ),
    ]
