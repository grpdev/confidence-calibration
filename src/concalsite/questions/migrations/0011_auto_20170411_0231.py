# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0010_auto_20170411_0226'),
    ]

    operations = [
        migrations.AddField(
            model_name='famouspeople',
            name='status',
            field=models.BooleanField(verbose_name='Статус', default=True),
        ),
        migrations.AddField(
            model_name='geographicobject',
            name='status',
            field=models.BooleanField(verbose_name='Статус', default=True),
        ),
    ]
