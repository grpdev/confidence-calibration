import requests
from django.core.management.base import BaseCommand
from questions.models import State


class Command(BaseCommand):

    def handle(self, *args, **options):
        url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'
        query = '''SELECT DISTINCT ?countryLabel ?headOfStateLabel
        {
           ?country wdt:P31 wd:Q6256;
           wdt:P35 ?headOfState .
           SERVICE wikibase:label { bd:serviceParam wikibase:language "ru" }
        }
        ORDER BY ?countryLabel
        LIMIT 130
        '''
        data = requests.get(url, params={'query': query, 'format': 'json'}).json()
        for item in data['results']['bindings']:
            # FIXME: некоторые имена с запятой
            State.objects.update_or_create(
                name=item['countryLabel']['value'],
                head=item['headOfStateLabel']['value']
            )
