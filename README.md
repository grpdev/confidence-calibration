# Установка для разработки #

### Создание телеграм-бота ###
1. Инструкция https://core.telegram.org/bots#3-how-do-i-create-a-bot
2. Нужно запомнить токен


### Настройка pythonanywhere.com ###
1. Регистрация
2. Открыть вкладку с консолями. Выбрать bash.
3. Склонировать проект
4. Создать виртуальное окружение и активировать его
> mkvirtualenv concal --python=python3
> workon concal
5. Перейти в папку с проектом и установить зависимости:
> cd confidence-calibration
> pip install -r requirements.txt
6. Выбрать кладку "Web"(в браузере) и добавить новый проект. Ручная настройка. Настроить:
https://bitbucket.org/repo/goryo6/images/3221718160-Screenshot%20from%202017-02-21%2016:02:21.png
Вместо vonafor пишите свой ник
https://bitbucket.org/repo/goryo6/images/1334095284-Screenshot%20from%202017-02-21%2016:32:41.png
7. Записать в WSGI configuration file:

```
#!python
import os
import sys

path = '/home/<username>/confidence-calibration/src'  # Исправить username
if path not in sys.path:
    sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'concalsite.settings'
os.environ['SECRET_KEY'] = "<Секретный ключ>. Для разработки можно, что угодно написать"
os.environ['TELEGRAM_BOT_TOKEN'] = "<Телеграм-токен>"
os.environ['PRODUCT_HOST'] =  '<Хост на pythonanywhere>' # пример 'vonafor.pythonanywhere.com'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

```
8. Дополнительная настройка виртуального окружения. Изменить файл(не забыть нажать Save) https://www.pythonanywhere.com/user/<username>/files/home/<username>/.virtualenvs/concal/bin/postactivate?edit

Значения должны быть такими же как и в п.7
Чтобы проверить, нужно запустить новую консоль, активировать окружение(workon concal). Если ошибок не возникло, то все ок. Если синтаксическая ошибка, то следует сэкранировать символы(кавычки, знак доллара) в ключах
```
#!/bin/sh
export SECRET_KEY="<Секретный ключ>"
export TELEGRAM_BOT_TOKEN="<Телеграм-токен>"
export PRODUCT_HOST="<username>.pythonanywhere.com"

```
9. Вернуться в консоль. Перейти в папку с manage.py. Выполнить миграции и собрать статику
> ./manage.py migrate

Папка, в которую собирается статика определяется параметром STATIC_ROOT(находится в settings.py). Если по каким-то причинам директория по умолчанию не доступна, то в settings.py можно указать любую другую.
> ./manage.py collectstatic --noinput

### Настройка Телеграм веб-хука ###
Нужно составить URL и перейти по нему(выполняется единственный раз)
>https://api.telegram.org/bot<token>/setWebhook?url=https://<username>.pythonanywhere.com/telegram/bot/<token>/


### Запуск тестов ###
> ./manage.py test


### Заполнение таблиц данными###
Для того чтобы заполнить таблицы фактами, нужно выполнить следующие команды(каждая команда выполняется только один раз).

> python manage.py fillStateData

> python manage.py fillFamousPeople

> python manage.py fillGeographicObject

> python manage.py fillRivers

### Администрирование ###
Нужно создать суперпользователя (англ. superuser)
> python manage.py createsuperuser

При появлении запроса введите имя пользователя (строчными буквами, без пробелов), адрес электронной почты и пароль
> Username: admin

> Email address: admin@admin.com

> Password:

> Password (again):

> Superuser created successfully.


### Просмотр сайта ###
Чтобы просмотреть сайт, нажать на вкладу web, перейти по ссылке (Вместо username впишите ваш ник):

> http://username.pythonanywhere.com

Для просмотра администратора БД перейти по ссылке:

> http://username.pythonanywhere.com/admin


Текущая рабочая версия сайта:

>http://concal.pythonanywhere.com

>http://concal.pythonanywhere.com/admin


### Команды telegram-bot ###
 
 > /start - начало работы. Пользователю предоставляется выбрать тип вопросов;
 
 > /enroll - начать тест;
 
 > /stop - закончить тест;
 
 > /last_result - посмотреть последний результат;
 
 > /history - Посмотреть историю тестов (информация о 5 последних тестах).